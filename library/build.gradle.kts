import util.KmpTarget
import util.configureAllKmpTargets
import util.configureKmpTargets

plugins {
    id("kmp-conventions")
    id("android-library-conventions")
    id("gitlab-publishing-conventions")

    alias(libs.plugins.kotlin)
    alias(libs.plugins.publish)
    alias(libs.plugins.kotlinx.serialization)
}

kotlin {
    configureKmpTargets(
        KmpTarget.JVM(),
        KmpTarget.ANDROID(),
        KmpTarget.NATIVE(),
        KmpTarget.WASMJS()
    )

//    jvm()
//
//    androidTarget {
//        publishLibraryVariants("release")
//        compilations.all {
//            kotlinOptions {
//                jvmTarget = "1.8"
//            }
//        }
//    }
//
//    linuxX64()
//    linuxArm64()
//    mingwX64()
//
//    @OptIn(ExperimentalWasmDsl::class)
//    wasmJs {
//        browser()
//    }
//
//    @OptIn(ExperimentalKotlinGradlePluginApi::class)
//    applyDefaultHierarchyTemplate {
//        common {
//            group("allJvm") {
//                withJvm()
//                withAndroidTarget()
//            }
//
//            group("notJvm") {
//                withNative()
//                withWasm()
//            }
//        }
//    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(libs.kotlinx.coroutines.core)
                implementation(libs.kotlinx.serialization.json)

                implementation(libs.ktor.core)
                implementation(libs.ktor.client.content.negotiation)
                implementation(libs.ktor.serialization.kotlinx.json)
            }
        }

        val allJvmMain by getting {
            dependencies {
                implementation(libs.ktor.client.cio)
                implementation(libs.newpipeextractor)
            }
        }

        val linuxX64Main by getting {
            dependencies {
                implementation(libs.ktor.client.curl)
            }
        }

        val linuxArm64Main by getting {
            dependencies {
                implementation(libs.ktor.client.cio)
            }
        }

        val mingwMain by getting {
            dependencies {
                implementation(libs.ktor.client.winhttp)
            }
        }
    }
}

mavenPublishing {
    coordinates("dev.toastbits", "ytm-kt")
}
